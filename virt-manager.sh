#!/bin/sh

docker rm -f virt-manager
docker run --name virt-manager -e DARK_MODE=false -e HOSTS="['qemu:///system']" -v /var/run/libvirt/libvirt-sock:/var/run/libvirt/libvirt-sock -v /var/lib/libvirt/images:/var/lib/libvirt/images --device /dev/kvm:/dev/kvm  -p 8185:80 mber5/virt-manager:latest

