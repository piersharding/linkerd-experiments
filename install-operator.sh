#!/bin/sh
set -x

cd ~/code/go/src/gitlab.com/ska-telescope/ska-tango-operator

for ctx in west east; do \
kubectl --context=kind-${ctx} create namespace ska-tango-operator || true ; \
helm upgrade --kube-context=kind-${ctx} --install test \
--set operatorConfig.logLevel=INFO --set controllerManager.replicas=1 \
--set controllerManager.manager.image.tag=0.9.3 --set kubernetesClusterDomain=cluster.local \
 ./charts/ska-tango-operator/  --namespace ska-tango-operator; \
done

