# Linkerd Multicluster Experimentation

Deploy Linkerd multicluster to support Tango Controls

## Summary

This repo provides a cooked example of setting up east and west clusters connected with Linkerd.  On top of this the tango Controls operator is deployed with a simple example that shows how to securely connect two K8s clusters together to enable cross cluster tango controls traffic.

To use this repository:
```
$ mkdir -p ~/code/go/src/gitlab.com/ska-telescope
$ cd ~/code/go/src/gitlab.com/ska-telescope && git clone git@gitlab.com:ska-telescope/ska-tango-operator.git && cd -
$ git clone git@gitlab.com:piersharding/linkerd-experiments.git
$ cd linkerd-experiments
$ make install-linkerd # install the linkerd tools
$ make create-ca # create shared CA between clusters
$ make cache REGISTRY_PROXY_USERNAME=<your docker user> REGISTRY_PROXY_PASSWORD=<password> # setup a registry cache so that you don't get hit by the Docker pull limit
$ make recycle # delete/recreate k8s clusters on Kind, link together, deploy tango operator, and bootstrap tango example
```

Once up and running, test using the `itango` Pod running in the West cluster:
```
In [11]: ds = Device("test/power_supply/1")

In [12]: ds.status()
Out[12]: 'The device is in STANDBY state.'

In [13]: data = ds.read_attribute("Voltage")

In [14]: data.value
Out[14]: 240.0
```
