#!/bin/sh

CERTS=linkerd-certs

#linkerd install \
#  --identity-trust-anchors-file ca.crt \
#  --identity-issuer-certificate-file issuer.crt \
#  --identity-issuer-key-file issuer-key-ssl.pem \
#  | kubectl apply -f -



linkerd install \
--set-file identityTrustAnchorsPem=${CERTS}/ca.crt \
--set-file identity.issuer.tls.crtPEM=${CERTS}/issuer.crt \
--set-file identity.issuer.tls.keyPEM=${CERTS}/issuer-private.pem \
  | kubectl apply -f -

