# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

export PATH := $(PATH):/home/piers/.linkerd2/bin
# PATH=$(PATH):/home/piers/.linkerd2/bin

KUBE_NAMESPACE ?= default
MINIKUBE_HOME ?= $(HOME)
CACHE_NAME=registry_cache
REGISTRIES=k8s.gcr.io gcr.io quay.io registry.gitlab.com docker.elastic.co artefact.skao.int
CACHE_DATA=$(MINIKUBE_HOME)/.minikube/$(CACHE_NAME)
REGISTRY_NAME=registry
REGISTRIES=k8s.gcr.io gcr.io quay.io registry.gitlab.com docker.elastic.co artefact.skao.int
REGISTRY_PROXY_USERNAME ?=
REGISTRY_PROXY_PASSWORD ?=

BASE = $(shell pwd)
IP=$(shell (ip a 2> /dev/null || ifconfig) | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)

CLUSTER_DOMAIN ?= cluster.local
CA_DIR ?= linkerd-certs


.DEFAULT_GOAL := help

.PHONY: cluster-up

# define private rules and additional makefiles
-include PrivateRules.mak

vars:
	@echo CA_DIR=$(CA_DIR)
	@echo CLUSTER_DOMAIN=$(CLUSTER_DOMAIN)
	@echo PATH=$(PATH)
	@echo IP=$(IP)

install-linkerd:  # install the linkerd client
	curl --proto '=https' --tlsv1.2 -sSfL https://run.linkerd.io/install | sh
	linkerd version --client

linkerd-setup:  # setup linkerd infra
	# first, install the Linkerd CRDs in both clusters
	make east
	linkerd install --crds | kubectl apply -f -
	make west
	linkerd install --crds | kubectl apply -f -

	# then install the Linkerd control plane in both clusters
	make east
	linkerd install \
	--identity-trust-anchors-file $(CA_DIR)/ca.crt \
	--identity-issuer-certificate-file $(CA_DIR)/issuer.crt \
	--identity-issuer-key-file $(CA_DIR)/issuer-private.pem \
	| kubectl apply -f -
	make west
	linkerd install \
	--identity-trust-anchors-file $(CA_DIR)/ca.crt \
	--identity-issuer-certificate-file $(CA_DIR)/issuer.crt \
	--identity-issuer-key-file $(CA_DIR)/issuer-private.pem \
	| kubectl apply -f -


# linkerd install \
#   --identity-trust-anchors-file ca-ssl.crt \
#   --identity-issuer-certificate-file issuer.crt \
#   --identity-issuer-key-file issuer-key-ssl.pem \
#   | kubectl apply -f -

# we don't actualy need to install VIZ
	# # install linkerd viz
	# for ctx in west east; do \
	# linkerd --context=kind-$${ctx} viz install | \
	# 	kubectl --context=kind-$${ctx} apply -f - || break; \
	# done

	# make east
	# kubectl wait --namespace linkerd-viz \
	# 				--for=condition=ready pod \
	# 				--selector=linkerd.io/proxy-deployment=prometheus \
	# 				--timeout=300s

	# kubectl wait --namespace linkerd-viz \
	# 				--for=condition=ready pod \
	# 				--selector=linkerd.io/proxy-deployment=metrics-api \
	# 				--timeout=300s

	# make west
	# kubectl wait --namespace linkerd-viz \
	# 				--for=condition=ready pod \
	# 				--selector=linkerd.io/proxy-deployment=prometheus \
	# 				--timeout=300s

	# kubectl wait --namespace linkerd-viz \
	# 				--for=condition=ready pod \
	# 				--selector=linkerd.io/proxy-deployment=metrics-api \
	# 				--timeout=300s

	# Check linkerd in each cluster
	for ctx in west east; do \
	echo "Checking cluster: $${ctx} ........."; \
	linkerd --context=kind-$${ctx} check || break; \
	echo "-------------"; \
	done

linkerd-prepare:  # prepare linkerd infra
	for ctx in west east; do \
	echo "Installing on cluster: $${ctx} ........."; \
	linkerd --context=kind-$${ctx} multicluster install | \
		kubectl --context=kind-$${ctx} apply -f - || break; \
	echo "-------------"; \
	done

	for ctx in west east; do \
	echo "Checking gateway on cluster: $${ctx} ........."; \
	kubectl --context=kind-$${ctx} -n linkerd-multicluster \
		rollout status deploy/linkerd-gateway || break; \
	echo "-------------"; \
	done

	for ctx in west east; do \
	printf "Checking cluster: ${ctx} ........."; \
	while [ "$(kubectl --context=${ctx} -n linkerd-multicluster get service -o 'custom-columns=:.status.loadBalancer.ingress[0].ip' --no-headers)" = "<none>" ]; do \
		printf '.'; \
		sleep 1; \
	done; \
	printf "\n"; \
	done

linkerd-link-east-to-west:  # link linkerd infra east to west
	linkerd --context=kind-east multicluster link --cluster-name kind-east | \
	kubectl --context=kind-west apply -f -
	sleep 30
	linkerd --context=kind-west multicluster check
	linkerd --context=kind-west multicluster gateways

linkerd-link-west-to-east:  # link linkerd infra west to east
	linkerd --context=kind-west multicluster link --cluster-name kind-west | \
	kubectl --context=kind-east apply -f -
	sleep 30
	linkerd --context=kind-east multicluster check
	linkerd --context=kind-east multicluster gateways

linkerd-check:
	linkerd --context=kind-west multicluster check
	linkerd --context=kind-west multicluster gateways

curl-east: east
	kubectl -n default apply -f service-east-to-west.yaml
	kubectl -n default apply -f curl.yaml
	sleep 3
	kubectl wait --namespace default \
					--for=condition=ready pod \
					--selector=app=curl \
					--timeout=300s
	kubectl -n default exec \
	  $$(kubectl -n default get pods -l app=curl -o name | head -1) -c curlserver \
	  -i -- sh -c "curl -v http://ping-service-kind-west.default.svc.cluster.local"
	kubectl -n default exec \
	  $$(kubectl -n default get pods -l app=curl -o name | head -1) -c curlserver \
	  -i -- sh -c "curl -v http://ping-service-link.default.svc.cluster.local"

clean-curl-east: east
	kubectl get pod
	kubectl delete -f curl.yaml

curl-west: west
	# special service so that name is set for local curl with no cluster suffix
	kubectl -n default apply -f service-west-to-east.yaml
	kubectl -n default apply -f curl.yaml
	sleep 3
	kubectl wait --namespace default \
					--for=condition=ready pod \
					--selector=app=curl \
					--timeout=300s
	kubectl -n default exec \
	  $$(kubectl -n default get pods -l app=curl -o name | head -1) -c curlserver \
	  -i -- sh -c "curl -v http://ping-service-kind-east.default.svc.cluster.local"
	kubectl -n default exec \
	  $$(kubectl -n default get pods -l app=curl -o name | head -1) -c curlserver \
	  -i -- sh -c "curl -v http://ping-service-link.default.svc.cluster.local"

clean-curl-west: west
	kubectl -n default get pod
	kubectl -n default delete -f curl.yaml

label-ping-east:
	kubectl --context=kind-east label svc -n default ping-service mirror.linkerd.io/exported=true


label-ping-west:
	kubectl --context=kind-west label svc -n default ping-service mirror.linkerd.io/exported=true


down: cluster-down ## teardown clusters

up: ## build clusters, setup everything
	make cache
	make cluster-up
	make install-metallb
	make install-ping
	make install-linkerd
	make linkerd-setup
	make linkerd-prepare
	make linkerd-link-east-to-west
	make linkerd-link-west-to-east
	make label-ping-east
	make label-ping-west

recycle:
	make down
	make up
	make curl-west
	make curl-east
	make operator
	make databaseds-west
	make databaseds-east-to-west
	make devices-east

OPERATOR_DIR ?= ~/code/go/src/gitlab.com/ska-telescope/ska-tango-operator
OPERATOR_TAG ?= 0.9.3
LOG_LEVEL ?= DEBUG
operator: # install tango operator everywhere
	cd $(OPERATOR_DIR); \
	for ctx in west east; do \
	kubectl --context=kind-$${ctx} create namespace ska-tango-operator || true ; \
	helm upgrade --kube-context=kind-$${ctx} --install test \
	--set operatorConfig.logLevel=$(LOG_LEVEL) --set controllerManager.replicas=1 \
	--set controllerManager.manager.image.tag=$(OPERATOR_TAG) --set kubernetesClusterDomain=cluster.local \
	./charts/ska-tango-operator/  --namespace ska-tango-operator; \
	done
	sleep 10
	for ctx in west east; do \
	kubectl --context=kind-$${ctx} -n ska-tango-operator wait --for=condition=available deployment.v1.apps/test-ska-tango-operator-controller-manager --timeout=180s; \
	done

databaseds-west: west # deploy the databaseds in the west cluster and expose
	kubectl -n default apply -f databaseds.yml
	sleep 3
	kubectl --namespace=default wait --for=condition=ready --timeout=180s $$(kubectl --namespace=default get pod -l component=itango-console -o name)
	kubectl label svc -n default tango-databaseds mirror.linkerd.io/exported=true

itango: ## itango into westSS
	kubectl --context=kind-west -n default \
	exec -it ska-tango-base-itango-console \
	-c itango -- itango3

devices-east: east # deploy the devices in the east cluster and expose
	kubectl --context=kind-east -n default apply -f devices.yml
	sleep 10
	kubectl --context=kind-west -n default apply -f west-device-services.yml
	# expose all the deviceservers
	# kubectl --context=kind-east label svc -n default deviceserver-asynctabata-tabata mirror.linkerd.io/exported=true
	kubectl --context=kind-east label svc -n default deviceserver-powersupply-test mirror.linkerd.io/exported=true
	kubectl --context=kind-east label svc -n default deviceserver-powersupply-test2 mirror.linkerd.io/exported=true

databaseds-east-to-west: east
	kubectl -n default apply -f databaseds-service-east-to-west.yaml
	kubectl -n default apply -f curl.yaml
	sleep 3
	kubectl wait --namespace default \
					--for=condition=ready pod \
					--selector=app=curl \
					--timeout=300s
	kubectl -n default exec \
	  $$(kubectl -n default get pods -l app=curl -o name | head -1) -c curlserver \
	  -i -- sh -c "nc -zv tango-databaseds-kind-west.default.svc.cluster.local 10000"
	kubectl -n default exec \
	  $$(kubectl -n default get pods -l app=curl -o name | head -1) -c curlserver \
	  -i -- sh -c "nc -zv tango-databaseds.default.svc.cluster.local 10000"

cache: ## docker image caching proxy
	# docker rm -f proxy || true
	docker run -d --name proxy --restart=always --net=kind \
	-e REGISTRY_PROXY_USERNAME=$(REGISTRY_PROXY_USERNAME) \
	-e REGISTRY_PROXY_PASSWORD=$(REGISTRY_PROXY_PASSWORD) \
	-e REGISTRY_PROXY_REMOTEURL=https://registry-1.docker.io registry:2 || true

	# mkdir -p $(CACHE_DATA)/docker_mirror_cache $(CACHE_DATA)/docker_mirror_certs; \
	# docker rm -f $(CACHE_NAME) 1>/dev/null 2>/dev/null || true; \
	# echo "Starting a new $(CACHE_NAME) in $(CACHE_DATA) ..."; \
	# docker run --name $(CACHE_NAME) \
	# 	-p 3128:3128 -e ENABLE_MANIFEST_CACHE=true \
	# 	-v $(CACHE_DATA)/docker_mirror_cache:/docker_mirror_cache \
	# 	-v $(CACHE_DATA)/docker_mirror_certs:/ca \
	# 	-e REGISTRIES="$(REGISTRIES)" \
	# 	-d docker.io/rpardini/docker-registry-proxy:$(CACHE_VERSION); \
	sleep 3

# https://blog.kubesimplify.com/getting-started-with-kind-creating-a-multi-node-local-kubernetes-cluster
# https://piotrminkowski.com/2021/07/08/kubernetes-multicluster-with-kind-and-submariner/
cluster-up:
	sed -i "s/apiServerAddress:.*/apiServerAddress: \"$(IP)\"/" kind-east.yaml
	kind create cluster --config kind-east.yaml
	make east
	kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.0/manifests/tigera-operator.yaml
	kubectl taint nodes east-control-plane node-role.kubernetes.io/control-plane-
	# kubectl taint nodes east-control-plane node-role.kubernetes.io/master-
	sed -i "s/apiServerAddress:.*/apiServerAddress: \"$(IP)\"/" kind-west.yaml
	kind create cluster --config kind-west.yaml
	make west
	kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.0/manifests/tigera-operator.yaml
	# create the calico deployment in east
	make east
	kubectl apply -f east-calico.yaml
	# create the calico deployment in west
	make west
	kubectl apply -f west-calico.yaml
	kubectl taint nodes west-control-plane node-role.kubernetes.io/control-plane-
	# kubectl taint nodes west-control-plane node-role.kubernetes.io/master-
	docker ps -a

cluster-down: # tear down the clusters
	for i in `kind get clusters`; do \
	kind delete cluster --name $${i}; \
	done
	# make kind-network
	docker ps -a

west: # select west context
	kubectl config get-contexts
	kubectl cluster-info --context kind-west
	kubectl config use-context kind-west
	kubectl get nodes

east: # select east context
	kubectl cluster-info --context kind-east
	kubectl config use-context kind-east
	kubectl config get-contexts
	kubectl get nodes

install-ping: # create the ping service in each cluster
	make east
	kubectl apply -f ping.yaml
	make west
	kubectl apply -f ping.yaml

kind-network: # reconfigure kind network to /16
	docker rm -f proxy || true
	docker network rm kind
	docker network create --subnet 172.19.0.0/16 --driver bridge kind

install-metallb: # install metallb  in each cluster
	docker network inspect -f '{{.IPAM.Config}}' kind
	make east
	kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
	kubectl wait --namespace metallb-system \
					--for=condition=ready pod \
					--selector=app=metallb \
					--timeout=300s
	kubectl apply -f east-metallb.yaml
	make west
	kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
	kubectl wait --namespace metallb-system \
					--for=condition=ready pod \
					--selector=app=metallb \
					--timeout=300s
	kubectl apply -f west-metallb.yaml

clean-ca: # clean CA
	rm -rf $(CA_DIR)

create-ca: # create CA
	mkdir -p $(CA_DIR)
	cd $(CA_DIR)
	# Create CA private key
	if [ ! -f $(CA_DIR)/ca-private.pem ]; then \
		openssl ecparam -name prime256v1 -genkey -noout -out $(CA_DIR)/ca-private.pem; \
	fi

	# Create CA public key
	openssl ec -in  $(CA_DIR)/ca-private.pem -pubout -out  $(CA_DIR)/ca-public.pem
	# Create self signed CA certificate
	openssl req -x509 -new -key  $(CA_DIR)/ca-private.pem -days 3650 -out  $(CA_DIR)/ca.crt -subj "/CN=root.linkerd.${CLUSTER_DOMAIN}"

	# Create issuer private key
	if [ ! -f  $(CA_DIR)/issuer-private.pem ]; then \
		openssl ecparam -name prime256v1 -genkey -noout -out  $(CA_DIR)/issuer-private.pem; \
	fi

	# Create issuer public key
	openssl ec -in  $(CA_DIR)/issuer-private.pem -pubout -out  $(CA_DIR)/issuer-public.pem

	# Create certificate signing request (BUG: the extension added here will be ignored by the signing)
	openssl req -new -key  $(CA_DIR)/issuer-private.pem -out  $(CA_DIR)/issuer.csr -subj "/CN=identity.linkerd.${CLUSTER_DOMAIN}" \
		-addext basicConstraints=critical,CA:TRUE

	# Create issuer cert by signing request
	openssl x509 \
		-extfile /etc/ssl/openssl.cnf \
		-extensions v3_ca \
		-req \
		-in  $(CA_DIR)/issuer.csr \
		-days 730 \
		-CA  $(CA_DIR)/ca.crt \
		-CAkey  $(CA_DIR)/ca-private.pem \
		-CAcreateserial \
		-extensions v3_ca \
		-out  $(CA_DIR)/issuer.crt
	rm  $(CA_DIR)/issuer.csr

	ls -latr  $(CA_DIR)/*
