#!/bin/sh
virt-install \
-n k8s1 \
--description "k8s1" \
--os-type=Linux \
--os-variant=ubuntu22 \
--ram=4096 \
--vcpus=4 \
--disk path=/var/lib/libvirt/images/k8s1.img,bus=virtio,size=30 \
--graphics none \
--cdrom /home/piers/Downloads/ubuntu-22.04.2-live-server-amd64.iso \
--network bridge:br0
